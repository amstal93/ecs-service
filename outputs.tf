output "service_id" {
  description = "ECS Service id"
  value       = aws_ecs_service.this.id
}

output "security_group_id" {
  description = "Security group id"
  value       = aws_security_group.this.id
}

output "task_definition_id" {
  description = "Task definition id"
  value       = aws_ecs_task_definition.this.id
}

output "iam_role_arn" {
  description = "Iam role of the task"
  value       = aws_iam_role.task.arn
}

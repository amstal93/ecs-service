variable "app_name" {
  description = "Application Name"
  type        = string
}

variable "app_port" {
  description = "Port that the application should expose"
  type        = number
  default     = 80
}

variable "auto_scaling_cooldown_time" {
  description = "Cooldown time in ms to wait for scaling up or down by cpu and memory"
  type        = number
  default     = 300
}

variable "auto_scaling_cpu_threshold_up" {
  description = "Amount in % of cpu upper threshold"
  type        = number
  default     = 70
}

variable "auto_scaling_memory_threshold_up" {
  description = "Amount in % of memory upper threshold"
  type        = number
  default     = 70
}

variable "cluster_name" {
  description = "Name of the ECS cluster to deploy"
  type        = string
}

variable "desired_task_cpu" {
  description = "Task CPU"
  default     = 1024
}

variable "desired_task_memory" {
  description = "Task Memory"
  default     = 2048
}

variable "environment_vars" {
  description = "List of static environment vars, composed by env name and value"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "filter_subnets_by_tags" {
  description = "Filter subnets to deploy resources using tags"
  type = list(object({
    key    = string
    values = list(string)
  }))
  default = []
}
variable "hc_status_code" {
  description = "Health check status code"
  type        = number
  default     = null
}

variable "hc_route" {
  description = "Route/Path to target for health check"
  type        = string
  default     = null
}

variable "is_https" {
  description = "The load balancer is using https"
  type        = bool
  default     = null
}

variable "iam_policies" {
  description = "List of IAM policies arns to access aws resources from within the task"
  type        = list(string)
  default     = []
}

variable "iam_policies_for_execution" {
  description = "List of IAM policies arns to access aws resources to start the task like SSM, KMS or Secrets Manager"
  type        = list(string)
  default     = []
}

variable "image_repository" {
  description = "Repository of the image"
  type        = string
}

variable "image_tag" {
  description = "Image tag to be used on the task definition"
  type        = string
  default     = "latest"
}

variable "launch_type" {
  description = "ECS Service launch type"
  type        = string
  default     = "EC2"
  validation {
    condition     = contains(["EC2", "FARGATE"], var.launch_type)
    error_message = "Invalid Input. Valid inputs are EC2 or FARGATE."
  }
}

variable "load_balancer_name" {
  description = "Name of the load balancer to use for deploy"
  type        = string
  default     = ""
}

variable "max_tasks" {
  description = "Maximum number of running tasks"
  type        = number
}

variable "min_tasks" {
  description = "Minimum number of running tasks"
  type        = number
  default     = 1
}

variable "redirect_rules" {
  description = "Map of redirect rules for load balancer"
  type        = map(list(string))
  default = {
    path_patterns = []
    host_headers  = []
  }
}

variable "secrets" {
  description = "List of environment vars from aws ssm or kms, composed by env name and secret arn"
  type = list(object({
    name      = string
    valueFrom = string
  }))
  default = []
}

variable "vpc_id" {
  description = "Id of the VPC to deploy the resources"
  type        = string
}

data "template_file" "this" {
  template = file("${path.module}/templates/taskdefinition.tpl")

  vars = {
    container_name = var.app_name
    container_port = var.app_port
    environment    = jsonencode(var.environment_vars)
    image          = var.image_repository
    image_tag      = var.image_tag
    log_group      = aws_cloudwatch_log_group.this.name
    region         = data.aws_region.current.name
    secrets        = jsonencode(var.secrets)
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = var.app_name
  container_definitions    = data.template_file.this.rendered
  requires_compatibilities = ["EC2", "FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.desired_task_cpu
  memory                   = var.desired_task_memory
  task_role_arn            = aws_iam_role.task.arn
  execution_role_arn       = aws_iam_role.this.arn
}

resource "aws_lb_target_group" "this" {
  count       = var.load_balancer_name != "" ? 1 : 0
  name        = "${var.app_name}-lb-tg"
  port        = var.app_port
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.selected.id
  target_type = "ip"

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    matcher  = var.hc_status_code
    path     = var.hc_route
    port     = var.app_port
    timeout  = 30
    interval = 40
  }
}

locals {
  path_rules = length(lookup(var.redirect_rules, "path_patterns", []))
  host_rules = length(lookup(var.redirect_rules, "host_headers", []))
}

resource "aws_lb_listener_rule" "this" {
  count        = var.load_balancer_name != "" ? 1 : 0
  listener_arn = var.is_https ? data.aws_lb_listener.selected443[0].arn : data.aws_lb_listener.selected80[0].arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this[0].arn
  }

  # hosts header condition
  dynamic "condition" {
    for_each = local.host_rules > 0 ? [var.redirect_rules] : []
    content {
      host_header {
        values = condition.value["host_headers"]
      }
    }
  }

  # path patterns condition
  dynamic "condition" {
    for_each = local.path_rules > 0 ? [var.redirect_rules] : []
    content {
      path_pattern {
        values = condition.value["path_patterns"]
      }
    }
  }
}

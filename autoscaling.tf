# Auto Scaling Tasks
resource "aws_appautoscaling_target" "this" {
  max_capacity       = var.max_tasks
  min_capacity       = var.min_tasks
  resource_id        = "service/${var.cluster_name}/${var.app_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

# https://docs.aws.amazon.com/autoscaling/application/APIReference/API_PredefinedMetricSpecification.html
resource "aws_appautoscaling_policy" "cpu" {
  name               = "${var.app_name}-cpu"
  resource_id        = aws_appautoscaling_target.this.resource_id
  scalable_dimension = aws_appautoscaling_target.this.scalable_dimension
  service_namespace  = aws_appautoscaling_target.this.service_namespace
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = " ECSServiceAverageCPUUtilization"
    }
    target_value       = var.auto_scaling_cpu_threshold_up
    scale_in_cooldown  = var.auto_scaling_cooldown_time
    scale_out_cooldown = var.auto_scaling_cooldown_time
  }
}

resource "aws_appautoscaling_policy" "memory" {
  name               = "${var.app_name}-memory"
  resource_id        = aws_appautoscaling_target.this.resource_id
  scalable_dimension = aws_appautoscaling_target.this.scalable_dimension
  service_namespace  = aws_appautoscaling_target.this.service_namespace
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = " ECSServiceAverageMemoryUtilization"
    }
    target_value       = var.auto_scaling_memory_threshold_up
    scale_in_cooldown  = var.auto_scaling_cooldown_time
    scale_out_cooldown = var.auto_scaling_cooldown_time
  }
}
